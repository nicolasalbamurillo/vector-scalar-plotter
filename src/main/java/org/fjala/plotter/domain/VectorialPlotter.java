package org.fjala.plotter.domain;

import javax.imageio.ImageIO;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.function.BiFunction;

public class VectorialPlotter {

    private static final Color AXIS_COLOR = Color.BLACK;
    private static final Color[] ZONE_COLORS = {
            new Color(21, 68, 122),
            new Color(23, 145, 145),
            new Color(233, 98, 40),
            new Color(242, 222, 182),
            new Color(204, 67, 68)
    };
    private static final float DELTA = 0.001F;

    private final int imageWidth;
    private final int imageHeight;
    private final int realWidth;
    private final int realHeight;
    private final int n;
    private final float minX;
    private final float maxX;
    private final float minY;
    private final float maxY;
    private final float arrowRatio;

    private BufferedImage image;
    private Graphics2D g;

    public VectorialPlotter(int imageWidth, int imageHeight, int realWidth, int realHeight, int n, float minX, float maxX, float minY, float maxY, float arrowRatio) {
        this.realHeight = realHeight;
        this.realWidth = realWidth;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.n = n;
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.arrowRatio = arrowRatio;
    }

    public void prepare() {
        this.image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        this.g = this.image.createGraphics();

        g.setStroke(new BasicStroke(imageWidth/1000F));
        Font stringFont = new Font( "SansSerif", Font.PLAIN, imageHeight/200 );
        g.setFont(stringFont);
        printBackGround(Color.WHITE);

        drawAxis();
        drawMarks(realWidth / 20F);
    }

    private void drawAxis() {
        g.setColor(AXIS_COLOR);
        float middleX = realWidth / 2F;
        float middleY = realHeight / 2F;

        drawLine(0, -middleY, 0, middleY);
        drawLine(-middleX, 0, middleX, 0);
    }

    private void drawMarks(float steps) {
        float lineLength = realWidth / 300F;
        float textSpace = realHeight / 600F;
        drawString("0", 0 + textSpace, textSpace);
        for (float i = steps; i < realWidth/2F; i+=steps) {
            drawLine(i, lineLength, i, -lineLength);
            drawString(String.format("%.2f", i), i + textSpace, textSpace);
            drawLine(-i, lineLength, -i, -lineLength);
            drawString(String.format("%.2f",-i), -i + textSpace, textSpace);
        }

        for (float i = steps; i < realHeight/2F; i+=steps) {
            drawLine(lineLength, i, -lineLength, i);
            drawString(String.format("%.2f",i), textSpace, i + textSpace);
            drawLine(lineLength, -i, -lineLength, -i);
            drawString(String.format("%.2f",-i), textSpace, -i + textSpace);
        }
    }

    public void plot(BiFunction<Float, Float, Vector2D> function) {
        float stepX = Math.abs(minX - maxX) / (float) n;
        float stepY = Math.abs(minY - maxY) / (float) n;
        for (float i = minX; i <= maxX; i+= stepX) {
            for (float j = minY; j <= maxY; j+= stepY) {
                Vector2D result = function.apply(i, j);
                g.setColor(getColorByZone(i, j));
                drawArrow(i, j, arrowRatio * result.getX() + i, arrowRatio * result.getY() + j);
                g.setColor(Color.RED);
                drawLine(i, j, i, j);
            }
        }
    }

    private Color getColorByZone(float x, float y) {
        float r = (float) Math.sqrt(x * x + y * y);
        float range = Math.abs(maxX);
        for (int i = ZONE_COLORS.length - 1; i >= 1; i--) {
            if (r >= range * ((float)i/ZONE_COLORS.length)) {
                return ZONE_COLORS[i];
            }
        }
        return ZONE_COLORS[0];
    }

    private void drawArrow(float x1, float y1, float x2, float y2) {
        if(equals(x1, x2) && equals(y1, y2)) {
            return;
        }
        float d = realWidth / 200F;
        float h = realWidth / 300F;
        float dx = x2 - x1;
        float dy = y2 - y1;
        float hypotenuse = (float) Math.sqrt(dx * dx + dy * dy);

        float x = hypotenuse - d;
        float ym = h;
        float yn = -h;

        float sin = dy / hypotenuse;
        float cos = dx / hypotenuse;

        float xmRotated = x*cos - ym*sin + x1;
        float ymRotated = x*sin + ym*cos + y1;
        float xnRotated = x * cos - yn * sin + x1;
        float ynRotated = x * sin + yn * cos + y1;

        drawLine(x2, y2, xmRotated, ymRotated);
        drawLine(x2, y2, xnRotated, ynRotated);
        drawLine(x1, y1, x2, y2);
    }

    private boolean equals(float a, float b) {
        return Math.abs(a - b) < DELTA;
    }

    private void drawString(String text, float x, float y) {
        g.drawString(text, getImageXFrom(x), getImageYFrom(y));
    }

    private void drawLine(float x1, float y1, float x2, float y2) {
        g.drawLine(getImageXFrom(x1), getImageYFrom(y1), getImageXFrom(x2), getImageYFrom(y2));
    }

    private int getImageXFrom(float x) {
        return (int) (x * imageWidth / realWidth) + imageWidth / 2;
    }

    private int getImageYFrom(float y) {
        return (int) (-y * imageHeight / realHeight) + imageHeight / 2;
    }

    private void printBackGround(Color color) {
        g.setColor(color);
        g.fillRect(0, 0, imageWidth, imageHeight);
    }

    public void save(String pngPath) throws IOException {
        File file = new File(pngPath);
        ImageIO.write(this.image, "png", file);
    }

    public BufferedImage getImage() {
        return image;
    }
}
