package org.fjala.plotter.domain;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.function.BiFunction;

public class ScalarPlotter {

    private final int imageWidth;
    private final int imageHeight;
    private final int realWidth;
    private final int realHeight;
    private BufferedImage image;
    private Graphics2D g;

    public ScalarPlotter(int imageWidth, int imageHeight, int realWidth, int realHeight) {
        this.realHeight = realHeight;
        this.realWidth = realWidth;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
    }

    public void prepare() {
        this.image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        this.g = this.image.createGraphics();
    }

    private float getUFrom(float x) {
        int middleWidth = imageWidth / 2;
        return (x + middleWidth);
    }

    private float getVFrom(float y) {
        int middleHeight = imageHeight / 2;
        return (-y + middleHeight);
    }

    private float mapFunctionImageSizeToRealSize(BiFunction<Float, Float, Float> function, int x, int y) {
        float realX = getRealX(x);
        float realY = getRealY(y);
        return function.apply(realX, realY);
    }

    private float getRealX(int x) {
        return x * realWidth / (float) imageWidth;
    }

    private float getRealY(int y) {
        return y * realHeight / (float) imageHeight;
    }

    public void plot(BiFunction<Float, Float, Float> function) {
        int domainFrom = -this.imageWidth / 2;
        int domainTo = -domainFrom;

        for (int x = domainFrom; x <= domainTo; x += 1) {
            for (int y = domainFrom; y <= domainTo; y+= 1) {
                float z = mapFunctionImageSizeToRealSize(function, x, y);
                g.setColor(getColorFrom(z));
                int u = (int) getUFrom(x);
                int v = (int) getVFrom(y);
                g.drawLine(u, v, u, v);
            }
        }
    }

    private Color getColorFrom(float z) {
        z = Math.abs(z);
        int map;
        if (z <= 0.5) {
            map = (int) (255 * z * 2);
            return new Color(255-map, map, 0);
        }
        map = (int) (255 * z);
        return new Color(0, 255-map, map);
    }

    public void save(String pngPath) throws IOException {
        File file = new File(pngPath);
        ImageIO.write(this.image, "png", file);
    }

    public BufferedImage getImage() {
        return image;
    }
}
