package org.fjala.plotter.domain;

public class Vector2D {
    private final float x;
    private final float y;

    public Vector2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
