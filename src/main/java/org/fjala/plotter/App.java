package org.fjala.plotter;

import org.fjala.plotter.domain.ScalarPlotter;
import org.fjala.plotter.domain.Vector2D;
import org.fjala.plotter.domain.VectorialPlotter;
import org.fjala.plotter.util.GifSequenceWriter;

import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class App {

    /**
     * F(x, y) = <p(x, y), q(x, y)>
     */
    private static final BiFunction<Float, Float, Vector2D> A = (x, y) -> new Vector2D((float) (sin(y) + cos(y)), (float) (sin(x) - cos(x)));
    private static final BiFunction<Float, Float, Vector2D> B = (x, y) -> new Vector2D(y, -x);
    private static final BiFunction<Float, Float, Vector2D> C = (x, y) -> new Vector2D((float) cos(x + 2 * y), (float) sin(x - 2 * y));
    private static final BiFunction<Float, Float, Vector2D> D = (x, y) -> new Vector2D(x, y);

    public static void main(String[] args) throws IOException {
        vectorialPlot();
    }

    private static void vectorialPlot() throws IOException {
        int n = 60;
        float arrowRatio = 0.25F;
        int imageSize = 10000;
        int realSize = 1;
        float domain = 0.3F;

        VectorialPlotter plotter = new VectorialPlotter(imageSize, imageSize, realSize, realSize, n, -domain, domain, -domain, domain, arrowRatio);
        plotter.prepare();
        plotter.plot(D);

        plotter.save("vector-plot.png");
    }

    public static void scalarPlot() throws IOException {
        ScalarPlotter plotter = new ScalarPlotter(1000, 1000, 40, 40);

        List<BufferedImage> images = new LinkedList<>();

        int parts = 100;
        for (int i = 0; i <= parts; i++) {
            float o = i / (float) parts;
            BiFunction<Float, Float, Float> function = (x, y) -> (float) (sin(2 * PI * (x*y + o)));
            plotter.prepare();
            plotter.plot(function);
            images.add(plotter.getImage());
        }
        createGif("test.gif", 25, images, true);
    }

    private static void createGif(String path, int delay, List<BufferedImage> images, boolean loop) throws IOException {
        ImageOutputStream output = new FileImageOutputStream(new File(path));
        GifSequenceWriter writer = new GifSequenceWriter(output, images.get(0).getType(), delay, loop);

        for (BufferedImage image : images) {
            writer.writeToSequence(image);
        }

        writer.close();
        output.close();
    }
}
