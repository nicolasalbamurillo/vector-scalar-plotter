Muy buenas Trainer Ariel.

Mi base fue: https://www.geogebra.org/m/V9Afznt5

Algunos extras que hice:

- Tener una imagen de mejor calidad a partir de una conversion de coordenadas.
- Generar un gif a partir de una clase utilitaria que encontre en internet.
- Poder trabajar con distintos valores de proporciones.
- Dividir los vectores por zonas y asignarle un color diferente.
- Hacer la flecha

## Campo escalar

Tal vez el gif tarde un poco en cargar. 
El siguiente gif tiene valor 100 en la variable partes, tardo 2 minutos en generar y pesa 50MB:

https://drive.google.com/file/d/1lHCX8iWc-dTEOD6z_iXor2f8rvlE_Gio/view

![GIF](https://drive.google.com/uc?export=view&id=1lHCX8iWc-dTEOD6z_iXor2f8rvlE_Gio)

## Campo Vectorial

A = <sin(y) + cos(y), sin(x) - cos(x)>

https://drive.google.com/file/d/1iZ0DqbTv2uAJeqxWaot8eKtY7_gI22Y-/view?usp=sharing

![](https://drive.google.com/uc?export=view&id=1iZ0DqbTv2uAJeqxWaot8eKtY7_gI22Y-)

B = <y, -x>

https://drive.google.com/file/d/1xaG513_CUhVWHefv82A8181GXsotViki/view?usp=sharing

![](https://drive.google.com/uc?export=view&id=1xaG513_CUhVWHefv82A8181GXsotViki)

C = <cos(x + 2y), sin(x - 2y)

https://drive.google.com/file/d/1ez-4-833aIYBsEpENwlda5-gRy7tQsJm/view?usp=sharing

![](https://drive.google.com/uc?export=view&id=1ez-4-833aIYBsEpENwlda5-gRy7tQsJm)

D = <x, y>

https://drive.google.com/file/d/14V2nnFA3BQiuD5i9uP_KmB8qc2-Vw7R1/view?usp=sharing

![](https://drive.google.com/uc?export=view&id=14V2nnFA3BQiuD5i9uP_KmB8qc2-Vw7R1)

Tambien hice algunos experimentos:

https://drive.google.com/file/d/1Nqlvu0UNCIEMH3Ekt2I3mK1gKilxUB40/view?usp=sharing

![](https://drive.google.com/uc?export=view&id=1Nqlvu0UNCIEMH3Ekt2I3mK1gKilxUB40)
